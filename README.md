# definebot #

definebot is a discord bot with basic functions, primarily (actually, only) for use in Shado_Temple's discord server.

### Version History ###
v0.1
* added !roll, !throw, and admin commands

v0.2
* added time delays, delete commands, and some twitch-related functions to the bot


### Setup ###
* [Download discord.py](https://github.com/Rapptz/discord.py)
* Create a config.json in the same location as discordbot.py as follows:

```
#!json

{
    "password":"<password>",
    "email":"<email>",
    "userid":<bot owner user id>,
    "bbid":<blacklisted user id>,
    "botmakerid":<botmaker role id>,
    "achan": <bot testing channel id>,
    "source": "https://bitbucket.org/justinhsg/definebot",
    "personaltesting": <private testing server id>
}
```

* <password> and <email> are the login credentials of the discord bot
* <blacklisted user id> is a blacklisted user id (e.g. another bot)
* <botmaker role id> is the id of the role that is allowed to use admin commmands on the bot
* The list of roles can be found under message.author.roles, and user ids can be found under message.author.id
* The id of the role can be found under (role class).id

* You may want to create a defines.json file:

```
#!json

{
    "input1":"<output1>",
    "email":"<output2>",
}
```


* Run discordbot.py and you should see the following message:
```
Logged in as
<bot username>
<bot userid>
```



### Future Versions ###
* Make code more readable

### Thanks ###
Special thanks to Ortho and Bomb_Mask for help with coding