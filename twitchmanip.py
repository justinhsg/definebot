import json
import requests

def getFromTeam(teamname):
    headers = {'Accept': 'application/vnd.twitchtv.v3+json'}
    url     = "https://api.twitch.tv/api/team/"+teamname+"/all_channels.json"
    url2    = "https://api.twitch.tv/kraken/teams/"+teamname 
    rq      = requests.get(url, headers = headers)
    rj      = rq.json()
    rq2     = requests.get(url2, headers = headers)
    rj2     = rq2.json()
    team    = rj2["display_name"]
    retlist = []
    retdict = {}
    
    if "channels" in rj:
        chlist = rj["channels"]
        for i in chlist:
            if(i["channel"]["status"] == "live"):
                tdict = {}
                tdict["name"] = i["channel"]["display_name"]
                tdict["game"] = i["channel"]["meta_game"]
                tdict["viewers"] = i["channel"]["current_viewers"]
                tdict["title"] = i["channel"]["title"]
                tdict["url"] = i["channel"]["link"][18:]
                retlist.append(tdict)
    retdict["name"] = team
    retdict["channels"] = retlist
    return retdict

def getTwitchEmotes():
    url = "https://twitchemotes.com/api_cache/v2/global.json"
    rq = requests.get(url)
    rj = rq.json()
    return rj

def addFFZ(channelname):
    url = "http://api.frankerfacez.com/v1/room/"+channelname
    rq = requests.get(url)
    rj = rq.json()
    if "error" in rj:
        return
    else:
        with open("ffz.json", "r") as f:
            flist = json.load(f)
            if channelname in flist:
                return
            else:
                flist.append(channelname)
        
        with open("ffz.json", "w") as f:
            json.dump(flist, f, sort_keys = True, indent = 4, ensure_ascii = False)
        
        return getFFZEmotes()

def rmFFZ(channelname):
    channelname = str(channelname.lower())
    with open("ffz.json", "r") as f:
        flist = json.load(f)
    if str(channelname) in flist:
        flist.remove(channelname)
    with open("ffz.json", "w") as f:
        json.dump(flist, f, sort_keys = True, indent = 4, ensure_ascii = False)
    return getFFZEmotes()

    
def getFFZEmotes():
    with open("ffz.json", "r") as f:
        flist = json.load(f)
    allffz = []
    for ch in flist:
        url = "http://api.frankerfacez.com/v1/room/"+ch
        rq = requests.get(url)
        rj = rq.json()
        tffz = rj['sets'][str(rj['room']['set'])]['emoticons']
        allffz += tffz
    return allffz