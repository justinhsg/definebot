# -*- coding: utf-8 -*-
import discord
import json
import datetime
    
with open("config.json") as fin:
    rawjson = json.load(fin)
    password = rawjson["password"]
    email = rawjson["email"]


    
client = discord.Client()
client.login(email, password)

@client.event
def on_channel_update(channel):
    
    if(int(channel.id) == 139525192664875008):
        song = channel.topic.encode('utf-8')[13:]
        ctime = str(datetime.datetime.now())
        
        flist = []
        with open("track.json", "r") as f:
            flist = json.load(f)
            for i in xrange(len(flist)):
                if(flist[i]['song'] == song):
                    try:
                        client.send_message(channel,"Repeat found: "+ song +" was played " + str(i+1) + " song(s) ago")
                    except Exception as e:
                        print("Error ibukitrack" + str(e))
                        return
                    break
            flist.insert(0, {'time': ctime, 'song': song})
            flist = flist[:100]
        
        with open("track.json", "w") as f:
            json.dump(flist, f, sort_keys = True, indent = 4, ensure_ascii = False)
                
                
@client.event
def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

client.run()
