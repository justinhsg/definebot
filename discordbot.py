# -*- coding: utf-8 -*-
import discord
from random import randint
import json
import time
import re
import twitchmanip
    
with open("config.json") as fin:
    rawjson = json.load(fin)
    password = rawjson["password"]
    email = rawjson["email"]
    userid = rawjson["userid"]
    botBombid = rawjson["bbid"]
    botMakerid = rawjson["botmakerid"]
    achanid = rawjson["achan"]
    source = rawjson["source"]
    personalid = rawjson["personaltesting"]

emotejson = twitchmanip.getTwitchEmotes()['emotes']
allffz = twitchmanip.getFFZEmotes()

client = discord.Client()
client.login(email, password)
timer = {"roll" : -1, "throw": -1, "help": -1, "body": -1}
delay = {"roll" : 5, "throw": 20, "help": 60, "body": 60}
msgStack = []
replaceBotBomb = True
vQuit = False

#Lists FFZ Channels
def listFFZ(message):
    ffzchs = "```I am using FFZ emotes from these channels:\n"
    with open("ffz.json", "r") as f:
        flist = json.load(f)
    for ch in flist:
        ffzchs += ch+", "
    ffzchs = ffzchs[:-2] + "```"
    try:
        msgStack.append(client.send_message(message.channel, ffzchs.encode('utf-8')))
    except Exception as e:
        print("Error listFFZ: " + str(e))


#Does some of botbomb's stuff
def takeoverbb(message):
    global msgStack
    global emotejson
    messageContent = unicode(message.content)
    getintPattern = re.compile('[0-9]+')
    
    #F to C
    ftocPattern = re.compile('(^|\s)[0-9]+\ ?F($|\s)')
    ftocQuery = ftocPattern.search(messageContent)
    if(ftocQuery != None):
        rawF = int(getintPattern.search(ftocQuery.group(0)).group(0))
        convC = int((rawF-32)/1.8)
        try:
            msgStack.append(client.send_message(message.channel, str(rawF) + '°F = '+ str(convC)+'°C'))
        except Exception as e:
            print('Error FtoC: ' + str(e))
        
        
    #C to F
    ctofPattern = re.compile('(^|\s)[0-9]+\ ?C($|\s)')
    ctofQuery = ctofPattern.search(messageContent)
    if(ctofQuery != None):
        rawC = int(getintPattern.search(ctofQuery.group(0)).group(0))
        convF = int(rawC*1.8+32)
        try:
            msgStack.append(client.send_message(message.channel, str(rawC) + '°C = '+ str(convF)+'°F'))
        except Exception as e:
            print('Error CtoF: ' + str(e))
        
        
        
    #defines
    with open("defines.json") as fan:
        definedict = json.load(fan)
    for k in definedict:
        definePattern = re.compile('(^|\s)' + k + '($|\s)')
        defineQuery = definePattern.search(messageContent)
        if(defineQuery != None):
            try:
                msgStack.append(client.send_message(message.channel, definedict[k].encode('utf-8')))
            except Exception as e:
                print('Error define: ' + str(e))
            

    for k in emotejson:
        lowerk = k.lower()
        lowermsg = messageContent.lower()
        emotePattern = re.compile('(^|\s):' + lowerk + ':($|\s)')
        emoteQuery = emotePattern.search(lowermsg)
        if(emoteQuery != None):
            try:
                msgStack.append(client.send_message(message.channel, 'http://emote.3v.fi/2.0/{}.png'.format(str(emotejson[k]['image_id'])).encode('utf-8')))
            except Exception as e:
                print('Error emote: ' + str(e))
            
    
    for k in allffz:
        lowername = k['name'].lower()
        lowermsg = messageContent.lower()
        emotePattern = re.compile('(^|\s):' + lowername + ':($|\s)')
        emoteQuery = emotePattern.search(lowermsg)
        if(emoteQuery != None):
            try:
                if "2" in k['urls']:
                    msgStack.append(client.send_message(message.channel, 'https:{}'.format(k['urls']['2']).encode('utf-8')))
                else:
                    msgStack.append(client.send_message(message.channel, 'https:{}'.format(k['urls']['1']).encode('utf-8')))
            except Exception as e:
                print('Error ffzemote: ' + str(e))
    
    return
            
#checks if messager is an Admin (grants access to admin commands)
def isAdmin(user, server):
    global botMakerid
    global userid
    if(int(user.id) == int(userid)): return True
    if(int(user.id) == int(server.owner.id)): return True
    for rol in user.roles:
        if(int(rol.id) == botMakerid):
            return True
    return False
            
@client.event
def on_message(message):
    global timer
    global delay
    global userid
    global botBombid
    global botMakerid
    global achanid
    global source
    global msgStack
    global replaceBotBomb
    global vQuit
    # Blacklists definebot and botBomb
    if message.author == client.user or int(message.author.id) == botBombid:
        return
    
    messageContent = unicode(message.content)
    # !definelife checks if bot works. Only admins can use it
    if messageContent == '!definelife' and isAdmin(message.author, message.server):
        msgStack.append(client.send_message(message.channel, 'Yes I am alive'))
        return
    
    # !setRollDelay <int> sets the delay of !roll to <int> seconds. Default is 5. Only admins can use it.
    if messageContent.startswith('!setRollDelay') and (isAdmin(message.author, message.server)):
        rawDelay = messageContent[14:]
        try:
            rawDelay = max(int(rawDelay),1)
        except:
            rawDelay = 5
        try:
            delay["roll"] = int(rawDelay)
        except Exception as e:
            print("Error rolldelay" + str(e))
        return
    
    # !setThrowDelay <int> sets the delay of !throw to <int> seconds. Default is 20. Only admins can use it.
    if messageContent.startswith('!setThrowDelay') and (isAdmin(message.author, message.server)):
        rawDelay = messageContent[15:]
        try: 
            rawDelay = max(int(rawDelay),1)
        except: 
            rawDelay = 20
        try:
            delay["throw"] = rawDelay
        except Exception as e:
            print("Error throwdelay" + str(e))
        return

    # !roll <int> rolls from 1 to <int> inclusive. Default is 20.
    if messageContent.startswith('!roll'):
        rawRoll = messageContent[6:]
        if (time.clock() - timer["roll"]) > delay["roll"] or timer["roll"] == -1 or isAdmin(message.author, message.server):
            try:
                rawRoll = max(int(rawRoll),1)
            except:
                rawRoll = 20
            try:
                msgStack.append(client.send_message(message.channel,'{} rolled a {} out of {}'.format(message.author.mention(), randint(1, rawRoll), rawRoll)))
            except Exception as e:
                print('Error Roll' + str(e))
            timer["roll"]=time.clock()
            return

    # !throw <string> causes the bot to throw <string>. Throws a table when <string> == "table". Default is the user.
    if messageContent.startswith('!throw'):
        rawThrow = messageContent[7:]
        if((timer["throw"] == -1) or ((time.clock()-timer["throw"])>delay["throw"]) or (isAdmin(message.author, message.server))) :
            if(len(rawThrow) == 0):
                rawThrow = message.author.mention()
            elif (rawThrow.lower() == 'table'):
                rawThrow = '┻━┻'
            try:
                msgStack.append(client.send_message(message.channel, '(╯°□°）╯︵ {}'.format(rawThrow.encode('utf-8'))))
            except:
                try:
                    msgStack.append(client.send_message(message.channel, '(╯°□°）╯︵ {}'.format(message.author.mention())))
                except Exception as e:
                    print("Error throw" + str(e))
            timer["throw"] = time.clock()
            return

    # !THROW <string> causes the bot to angrily throw <string>. Throws a table when <string> == "table". Default is the user.
    if messageContent.startswith('!THROW'):
        rawThrow = messageContent[7:]
        if((timer["throw"] == -1) or ((time.clock()-timer["throw"])>delay["throw"]) or (isAdmin(message.author, message.server))) :
            if(len(rawThrow) == 0):
                rawThrow = message.author.mention()
            elif (rawThrow.lower() == 'table'):
                rawThrow = '┻━┻'
            else:
                rawThrow = rawThrow.upper()
            try:
                msgStack.append(client.send_message(message.channel, '(ノಠ益ಠ)ノ彡︵ {}'.format(rawThrow.encode('utf-8'))))
            except:
                try:
                    msgStack.append(client.send_message(message.channel, '(ノಠ益ಠ)ノ彡︵ {}'.format(message.author.mention())))
                except Exception as e:
                    print("Error THROW" + str(e))
            timer["throw"] = time.clock()
            return

    # !body gives the body discovery announcement from Danganronpa.
    if(messageContent == '!body'):
        if (time.clock() - timer["body"] > delay["body"]) or timer["body"] == -1 or isAdmin(message.author, message.server):
            bodyann = "BING BONG BON BONG! A body has been discovered! After a certain amount of time, which you may use however you like, the class trial will begin! :MonoLaugh:"
            try:
                msgStack.append(client.send_message(message.channel,bodyann))
                timer["body"] = time.clock()
            except Exception as e:
                print("Error body" + str(e))
            return
    
    
    
    # !definehelp gives the list of commands to use.
    if (messageContent == '!definehelp'):
        if(((time.clock() - timer["help"]) > delay["help"]) or (timer["help"] == -1) or isAdmin(message.author, message.server)):
            helpfile = "```\
            definebot commands\n\n\
!definehelp     : Gives list of discovered commands. "+str(delay["help"])+"s delay\n\n\
!roll <int>     : Rolls from 1 to <int> inclusive. \n\
                  Default <int> = 20. "+str(delay["roll"])+"s delay\n\n\
!throw <string> : Throws <string>. Throws you instead if bot doesn't like you\n\
                  Default <string> = <username>. "+str(delay["throw"])+"s delay\n\n\
!THROW <string> : Angrier version of !throw. Same attributes.\n\n\
!body           : Gives the body discovery announcement. "+str(delay["body"])+"s delay\n\n\
!listFFZ        : Gives FFZ channels that are added\n\n\
:<string>:      : Gives FFZ/Twitch Emotes if they exist or are added\n\n\
definebot is open-source, you just have to use find the code yourself :Kappa:\n\
definebot is made from discord.py, an opensource discord bot API. GIYBF\n\
===END===\
            ```"
            try:
               msgStack.append(client.send_message(message.author,helpfile))
            except Exception as e:
                print("Error help:" + str(e))
            timer["help"] = time.clock()
            return

    # !defineadmin gives the list of admin commands to use. Only whitelisted admins can use it.
    if (messageContent == '!defineadmin') and (isAdmin(message.author, message.server)):
        adminfile = "```\
        definebot admin commands\n\n\
!definelife           :Checks if definebot is alive.\n\n\
!setRollDelay <int>   :Sets the delay of the !roll command to <int>.\n\
                       Default <int> = 5\n\n\
!setThrowDelay <int>  :Sets the delay of the !throw/!THROW command to <int>.\n\
                       Default <int> = 20\n\n\
The next few commands can only be used in channel #bot:\n\n\
!getTeam <str>        :Gives the twitch channels of team <str> that are online\n\n\
!addFFZ <str>         :Adds FFZ emotes from <str>\n\n\
!removesFFZ <str>         :Removes FFZ emotes from <str>\n\n\
!definereplace        :Toggles the bot to give emotes if botBomb is dead\n\n\
!definesource         :Gives source of definebot and python API.\n\n\
!membersize           :Gives number of discord server members.\n\n\
!definedelete <str>   :Deletes the last <str> messages if <str> is an integer.\n\
                       Deletes all messages if <str> is 'all'\n\
                       Default <str> = 1\n\n\
!definequit           :Logs out definebot from discord\n\n\
===END===\
           ```"
        try:
            msgStack.append(client.send_message(message.channel,adminfile))
        except Exception as e:
            print("Error admin:" + str(e))
        return
        
    
    # !definesource gives the sources of the bot and API. Only usable within the whitelisted server.
    if(messageContent == '!definesource' and int(message.channel.id) == achanid):
        try:
            msgStack.append(client.send_message(message.channel, "definebot is found at: " + source + "\n Bot api is found at https://github.com/Rapptz/discord.py "))
        except Exception as e:
            print("Error source:" + str(e))
        return
    
    # !membersize gives the size of the discord server. Only usable within the whitelisted server.
    if(messageContent == '!membersize' and int(message.channel.id) == achanid):
        try:
            msgStack.append(client.send_message(message.channel, "There are " + str(len(message.server.members)) +" members on this server"))
        except Exception as e:
            print("Error membersize:" + str(e))
        return
        
        
    # !definequit stops the bot, and logs it out of discord. Only botMakers can use this.
    if(messageContent == '!definequit' and isAdmin(message.author, message.server)):
        try:
            print("Logging out...")
            vQuit = True
            client.logout()
            print("==============")
        except Exception as e:
            print("Error quitting: " + str(e))
        return
    
    # !definedelete causes the bot to remove the last few messages. Only botMakers can use this.
    if(messageContent.startswith('!definedelete') and isAdmin(message.author, message.server)):
        rawDelete = messageContent[14:]
        if(str(rawDelete.lower()) == str('all')):
            rawDelete = int(msgStack.__len__())
        try:
            rawDelete = max(min(int(msgStack.__len__()), int(rawDelete)),1)
        except:
            rawDelete = 1
        for x in xrange(rawDelete):
            try:
                client.delete_message(msgStack.pop())
            except Exception as e:
                print("Error rawDelete: " + str(e))
        return
    
    # toggle to take over some of primary bot's commands
    if(messageContent == "!definereplace" and int(userid) == int(message.author.id)):
        if(replaceBotBomb):
            try:
                msgStack.append(client.send_message(message.channel, "Not taking over some of BotBomb's commands"))
                replaceBotBomb = False
            except Exception as e:
                print("Error replace" + e)
        else:
            try:
                msgStack.append(client.send_message(message.channel, "Taking over some of BotBomb's commands"))
                replaceBotBomb = True
            except Exception as e:
                print("Error replace:" + str(e))
        return
        
    
            
    if(messageContent.startswith("!getTeam") and isAdmin(message.author, message.server)):
        rawTeams = messageContent[9:]
        rawTeams = rawTeams.lower()
        rdict = twitchmanip.getFromTeam(rawTeams)
        teamName = rdict['name']
        onlinelist = rdict['channels']
        if(onlinelist.__len__() == 0):
            teammsg = "There are no members from '" + teamName + "' that are online now"
        else:
            teammsg = "```\
Here are the members of '"+ teamName+"' that are online right now:\n"
            for i in onlinelist:
                teammsg = teammsg + "\
======================================\n\
Name   : "+i['name']+"\n\
Stream : "+i['title']+"\n\
Game   : "+i['game']+"\n\
Viewers: "+str(i['viewers'])+"\n\
URL    : "+i['url']+"\n"
            teammsg = teammsg+"```"    
        try:
            msgStack.append(client.send_message(message.channel, teammsg.encode('utf-8')))
        except Exception as e:
            print("Error teammsg: " + str(e))
        return
    
    if(messageContent.startswith("!addFFZ") and isAdmin(message.author, message.server)):
        rawCh = messageContent[8:]
        rawCh = rawCh.lower()
        allffz = twitchmanip.addFFZ(rawCh)
        listFFZ(message)
        return
    
    if(messageContent.startswith("!rmFFZ") and isAdmin(message.author, message.server)):
        rawCh = messageContent[7:]
        rawCh = rawCh.lower()
        allffz = twitchmanip.rmFFZ(rawCh)
        listFFZ(message)
        return
    
    if(messageContent == "!listFFZ"):
        listFFZ(message)
        return
    
    
    # checks if BotBomb is alive, then passes message to botbomb replacement
    for m in message.server.members:
        if (int(m.id) == int(botBombid)):
            if(m.status == 'offline' and replaceBotBomb):
                takeoverbb(message)
                return
            break
    if(int(message.server.id) == personalid): takeoverbb(message)

@client.event
def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

#The next two functions is in major testing phase, it should technically reconnect the client if there is a temporary loss in connection to the server.    
@client.event
def on_socket_closed():
    if(vQuit == False):
        retryconnection()
    else:
        print("Logged Out.")

def retryconnection():
    while True:
        print ("Trying to login")
        rclient = client
        rclient.login(email,password)
        rclient.run()
        if(rclient.is_logged_in):
            break
        else:
            time.sleep(600)

    

client.run()